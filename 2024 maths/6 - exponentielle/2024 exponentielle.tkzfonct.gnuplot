set table "2024 exponentielle.tkzfonct.table"; set format "%.5f"
set samples 200.0; plot [x=-2.000000000000000000:4.000000000000000000] ((exp((x*1)))-0)/4
