<html><head><meta charset='utf-8'><meta id="ForPhone" name="viewport" content="width=device-width, initial-scale=1.0"><title>Ma page Physique en BTS</title> </head>

  

  
  
<body>
  
  
# Ressources BTS Bâtiment

  <br>

<div> J'enseigne depuis quelques années la physique/chimie en BTS Bâtiment. La particularité de mes promotions : uniquement de l'alternance.
  <br>
  Vous trouverez sur cette page des cours / TP / exercices de physique chimie que je donne à ce niveau. Ces cours sont construits dans la mesure du possible sur des séances expérimentales et sur des thématiques liées au bâtiment.
  
  Pour retrouver les autres niveaux (maths / sciences en lycée pro), [cliquez sur ce lien](https://tmounier.forge.aeif.fr/maths-sciences/).</div>

  <br>
  
<p><strong>Les ressources  </strong></p>
  

<div> Les documents sont disponibles, la plupart du temps, au format .tex (LaTeX) ou au format .pdf pour une utilisation direct en cas de besoin.
</div>

---
  
## Première année

### Les cours + exercices  
  
1. [Grandeurs Physiques](https://forge.aeif.fr/tmounier/bts-tex/-/blob/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1/1%20-%202%20-%203%20-%20bases/2023%20Grandeurs%20Physique%20ch1.pdf)  
2. [Chiffres Significatifs](https://forge.aeif.fr/tmounier/bts-tex/-/blob/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1/1%20-%202%20-%203%20-%20bases/2023%20Chiffres%20Significatifs%20ch2.pdf)  
3. [Incertitudes](https://forge.aeif.fr/tmounier/bts-tex/-/blob/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1/1%20-%202%20-%203%20-%20bases/2023%20Incertitudes%20ch3.pdf)
4. [Masse volumique](https://forge.aeif.fr/tmounier/bts-tex/-/blob/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1/4%20-%20masse%20volumique/2023%20masse%20volumique.pdf) (application des points précédents)
5. [Statique des fluides](https://forge.aeif.fr/tmounier/bts-tex/-/blob/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1/5%20-%20statique%20des%20fluides/2023%20ch5%20statique%20des%20fluides.pdf)
6. Rappels de chimie
7. Oxydoréduction
8. Généralités sur les ondes

  
### Les séances expérimentales
  
En refonte 2023/2024  

  
Et la [base du dépôt](https://forge.aeif.fr/tmounier/bts-tex/-/tree/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS1) où vous trouverez les sources.


## Seconde année

### Les cours + exercices  
1. [Rappels de chimie](https://forge.aeif.fr/tmounier/bts-tex/-/tree/0e1beaf4ab650a952f0d804747ecb6c04e0da174/BTS2/Rappel%20chimie%20pour%20CCF)  (préparation de CCF)
2. Thermique
3. Acoustique
4. Eclairement

  
### Les séances expérimentales
  
En refonte 2023/2024  

Et la [base du dépôt](https://forge.aeif.fr/tmounier/bts-tex/-/tree/main/BTS2) où vous trouverez les sources.
  

## Des ressources .tex 
  
Afin d'ouvrir et de compiler les documents, j'ai aussi placé [à la racine du dépôt](https://forge.aeif.fr/tmounier/bts-tex/-/tree/0e1beaf4ab650a952f0d804747ecb6c04e0da174/) mes packages persos.


## Des ressources libres
  
  
En attente d'une plus profonde réflexion (et gestion des conflits), mes ressources sont distribuées sous licence 
  ![](https://forge.aeif.fr/tmounier/term-tex/-/raw/main/ccby.png)


  </body></html>