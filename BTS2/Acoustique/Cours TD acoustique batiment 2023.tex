\documentclass[12pt,a4paper,oneside,dvipsnames,table,svgnames,skins,theorems]{report}
\usepackage{ProfCollege}
\usepackage{tmbasev2} %pour le moment placé dans la racine du .tex ... !
\usepackage{postit}


\graphicspath{{C:/Users/Thom/Documents/latex nas/forge/bts-tex/images/acoustique/}}

\ordi{fixe} %pour fichier image
\matierechap{pc} %m pour maths, pc pour physique chimie, rien pour vierge
\version{2} %1 : PROF   2: ELEVE sans corrigé  3: corrigé des exos sans le cours
\setcounter{chapter}{1} %compteur de chapitre

\begin{document}

\setcounter{chapter}{2}
\chapter{Acoustique dans le bâtiment}

\section{Décibel et limites}

L'échelle des décibel est limitée : elle ne reproduit pas fidèlement l'oreille humaine. Un son de \SI{100}{\decibel} mesuré peut ne pas être ressenti à ce niveau par une oreille qui va avoir un comportement différent (filtrage) selon les fréquences.
\vspace{0.5cm}

Pour palier à ce problème, une version "corrigée" des décibel (il y en a en fait plusieurs) a été intégrée : le $dB(A)$. On cherche à reproduire la sensation de l'oreille, on pondère les bruits en affaiblissant les graves et aigus situés en dehors de la zone de sensibilité de l'oreille. \\
Toutes les fréquences ne sont pas pondérées de la même manière : 

\begin{center}
\includegraphics[scale=1]{ponderationdb.png}
\end{center}

Un bruit de fréquence \SI{500}{\hertz} ayant pour niveau \SI{66}{\decibel} deviendra un bruit ayant un niveau de \SI{62.8}{\decibel}(A) et ainsi de suite.

\section{Niveau Global}

\cadre{\begin{center}
\textbf{L'intensité sonore totale d'un son est égale à la somme des intensités sonores}
\end{center}}

\vspace{0.5cm}

Pour un son qui peut se décomposer comme $I_{totA} = I_{1A} + I_{2A} + ...$ on peut écrire le niveau.
\vspace{0.5cm}

Sachant que : {\large $L_{1A} = 10\log{\dfrac{I_{1A}}{I_0}}$} alors on a {\Large $I_{1A} = I_0 \times 10^{\dfrac{\scriptscriptstyle L_{1A}}{\scriptscriptstyle 10}}$}
\vspace{0.5cm}

On généralise la formule : \begin{Large}
 $\displaystyle L_{totA} = 10 \log{\dfrac{I_0 \times \sum 10^{\scriptscriptstyle \dfrac{L_{iA}}{\scriptscriptstyle 10}}}{I_0}} $

\end{Large}
\vspace{0.2cm}

Calculer l'intensité totale d'un son qui aurait pour composante les fréquences du tableau ci-dessus. Réponse : $L\approx \SI{73}{\decibel}$(A)

\section{Transformation d'énergie}


\begin{minipage}{0.6\textwidth}
Une onde sonore qui rencontre un obstacle (par exemple une paroi) subit une transformation : 
\begin{itemize}
\item Une partie est réfléchie ou absorbée (on va traiter de correction acoustique)
\item Une partie est transmise (on va traiter d'isolation acoustique)
\end{itemize}
\vspace{0.1cm}

Ces deux situations doivent être prises en compte pour faire de la correction acoustique.

\end{minipage}
\hspace{0.1cm}
\begin{minipage}{0.4\textwidth}
\begin{center}
\includegraphics[scale=0.7]{energiesonore.png}
\end{center}

\end{minipage}

\vspace{0.5cm}

Si le local est fermé les ondes vont rebondir et former des réflexions multiples. A chaque réflexion, l'onde perd une partie de son énergie.\\
Cela crée des échos dont l'intensité diminue progressivement.\\
La personne dans la pièce entend alors un son d'intensité décroissante pendant une certaine durée : c'est le temps de réverbération souvent noté $Tr$.

\vspace{0.1cm}

\cadre{$Tr$ est le temps mis par l'intensité sonore pour décroitre jusqu'à $10^{-6}$ de sa valeur initiale}
\vspace{0.1cm}

\subsection{Absorption}

Une partie est absorbée par toutes les parois. On définit le coefficient \mybox{{\Large $\alpha = \dfrac{I_A}{I}$}} valable pour une certaine fréquence. Il est influencé par :
\begin{itemize}
\item L'angle d'incidence de l'onde
\item La nature du matériau
\item La fréquence de l'onde
\end{itemize}

Remarque : le mobilier et les autres personnes contribuent à l'absorption du son.
\vspace{0.1cm}

On définit l'unité absorbante de surface : \mybox{ {\Large $A=\sum \alpha_i S_i$} }.
\vspace{0.1cm}

C'est à dire qu'on prend en compte tous les éléments de la pièce multipliés par leur surface pour donner cette valeur de A qui dépend donc de l'environnement.
\vspace{0.2cm}

\textbf{Remarque} : si la pièce contient des éléments absorbants n'étant pas des éléments de surface (mobilier, humains...) on a alors : \mybox{ {\Large $A=\sum ( \alpha_i S_i + \alpha_i n_i)$}} $n$ représentant les divers.
\newpage

\subsection{Calcul de Tr (Formule de Sabine)}

Cette formule empirique permet le calcul de $Tr$ :
\vspace{0.2cm}

\mybox{ {\Large $T_r = \dfrac{0.16 \times V}{A}$}} ou sous une autre forme :  \mybox{ {\Large $T_r = \dfrac{0.16 \times V}{\sum ( \alpha_i S_i + \alpha_i n_i)}$}}
\vspace{0.5cm}

Remarques :

\begin{itemize}
\item Les matériaux ont une réponse différente selon la fréquence du son, voir exercices.
\item Cette formule est empirique : elle fonctionne en général assez bien
\end{itemize}


\subsection{Transmission au sein d'une paroi}

Une paroi homogène transmet plus ou moins bien. On définit un coefficient de transmission $\tau$ : $$\tau = \dfrac{\text{énergie transmise}}{\text{énergie incidente}}$$
\vspace{0.5cm}

La capacité d'isolation de la paroi s'exprime grâce à l'indice d'affaiblissement acoustique : 
\vspace{0.1cm}

\begin{minipage}{0.6\textwidth}

{\Large $$R= 10\log{\dfrac{1}{\tau}} = L_{P1} - L_{P_2}$$}

Avec $R$ en décibel (\si{\decibel}). Cette valeur dépend de la fréquence.

\end{minipage}
\hspace{0.1cm}
\begin{minipage}{0.4\textwidth}
\begin{center}
\includegraphics[scale=1.25]{affaibacoustique.png}
\end{center}

\end{minipage}

\vspace{0.2cm}


Si la paroi n'est pas homogène il faut remplacer $\tau$ par un coefficient calculé : 

{\large  $$\tau_{moyen} = \dfrac{S_a \tau_a + S_b \tau_b + ...}{S_a + S_b + ...}$$}



\vspace{0.2cm}

Si les parois sont en série mais connue on peut appliquer la formule : \mybox{{\Large $\tau_{paroi} = \prod \tau_i$}}

\subsection{Loi des masses}

Plus la masse est importante, plus il sera difficile de faire vibrer la paroi. Meilleure devient l'isolation acoustique. On définie : $\sigma = \rho \times e$ avec $\rho$ la masse volumique et $e$ l'épaisseur.
\vspace{0.1cm}

Cet indice $\sigma$ sert dans des lois empiriques dépendant de sa valeur :
\begin{itemize}
\item $R=17\log{\sigma} + 4$ si $\sigma < \SI{150}{\kilo\gram\per\meter\squared}$
\item $R=40\log{\sigma} -46$ sinon.
\end{itemize}





\ifsectionexos
\newpage
\section{Exercices}
\Opensolutionfile{mycor}[ficcorex]

\exo{} Dans un établissement scolaire on dispose de deux salles neuves de dimensions $L=\SI{15}{\meter}$, $l=\SI{10}{\meter}$ et $h=\SI{3.2}{\meter}$. On procède à une mesure de $Tr$ sans tenir compte des ouvertures de la salle.
\begin{enumerate}
\item Rappeler la formule de Sabine
\item Donner la signification des termes de cette formule ainsi que leurs unités
\item Que représente le temps $Tr$ physiquement ?

Après mesure on a $Tr = \SI{2.2}{\second}$
\item En déduire la surface équivalente d'absorption

On va adapter une des deux salles en salle de concert et l'autre en salle de classe. Pour la salle de classe, le temps $Tr$ doit répondre au critère $Tr < \SI{0.5}{\second}$ et pour la salle de concert $Tr < \SI{1.5}{\second}$.\\
On recouvre les murs d'un matériau de coefficient d'absorption $\alpha_0 = 0.20$. Le plancher est négligé. Le plafond est quant à lui recouvert pour la salle de classe d'un matériau de coefficient $\alpha_1$ et pour la salle de concert $\alpha_2$.
\item Déterminer les valeurs de $\alpha_1$ et $\alpha_2$.
\end{enumerate}
\begin{correction}

\begin{enumerate}
\item Voir cours
\item Voir cours
\item Voir cours
\item $A =\dfrac{0.16V}{T_r} = \SI{34.9}{\meter\squared}$
\item Pour la salle de classe : \\
$A_P = \alpha_1 S_P =  150 \alpha_1 \si{\meter\squared}$ \\
En reprenant la formule de Sabine : $A = A_m + A_P =  32 + 150\alpha_1 = \dfrac{0.16V}{Tr}$. On isole et on obtient : $ \alpha_1 = \dfrac{\dfrac{0.16 \times 3.2 \times 10 \times 15}{0.5}-32}{150} \approx 0.81$

\vspace{0.1cm}

On reprend la même démarche pour l'autre et on obtient : $\alpha_2 = 0.128$

\end{enumerate}

\end{correction}


\finexo

\vspace{0.5cm}

\exo{} Une pièce a pour dimensions $L=\SI{6.0}{\meter}$, $l=\SI{4.0}{\meter}$ et $h=\SI{3.0}{\meter}$. On donne les informations suivantes :
\begin{itemize}
\item Le sol est composé de lino tel que $\alpha_1=0.10$
\item Les murs et les plafonds bruts : $\alpha_2 = 0.020$
\item Les fenêtres (2) ont une surface $S=_f\SI{3.0}{\meter\squared}$ chacune et $\alpha_3 = 0.030$
\item La porte est en bois, $S_p=\SI{3.0}{\meter\squared}$ avec $\alpha_4=0.030$
\item Les éléments incalculables augmentent la valeur de $A$ de 10\%
\end{itemize}
On demande :
\begin{enumerate}
\item Calculer $A$. (On pourra utiliser le tableau plus bas)
\item Utiliser la formule de Sabine pour calculer le temps de réverbération de cette pièce.

On ajoute une couche d'isolant sur le plafond telle que $\alpha_5=0.75$
\item Calculer la nouvelle valeur de A (On gardera le 10\% de la question 1)
\item Cette modification a t'elle permise de réduire de moitié le temps de réverbération ?
\end{enumerate}

\renewcommand{\arraystretch}{1.5}
\begin{tabularx}{\textwidth}{|C|C|C|C|}
\hline
Éléments & $\alpha_i$ & $S_i$ & $\alpha_i S_i$ \crh
Sol & & & \crh
Porte & & & \crh
Fenêtres & & & \crh
Murs & & & \crh
Plafond & & & \crh
& Total &  & \crh
& & 10\% & \crh
& & $A_1$ & \crh

\end{tabularx}
\vspace{0.1cm}
\begin{correction}
Synthèse dans un tableau :
\vspace{0.1cm}

\renewcommand{\arraystretch}{1.25}
\begin{tabularx}{\textwidth}{|C|C|C|C|}
\hline
Éléments & $\alpha_i$ & $S_i$ & $\alpha_i S_i$ \crh
Sol &0.10 & 24 & 2.4\crh
Porte & 0.030& 2& 0.060\crh
Fenêtres & 0.030&6 & 0.18\crh
Murs &0.020 & 52& 1.04\crh
Plafond &0.020 &24 & 0.48\crh
& Total & \SI{108}{\meter\squared} & \SI{4.16}{\meter\squared} \crh
& & 10\% & 0.416\crh
& & $A_1$ & 4.58\crh

\end{tabularx}
\vspace{0.1cm}

on a donc $Tr = \dfrac{0.16 \times 72}{4.58} \approx \SI{2.5}{\second}$
\vspace{0.2cm}

\renewcommand{\arraystretch}{1.25}
\begin{tabularx}{\textwidth}{|C|C|C|C|}
\hline
Éléments & $\alpha_i$ & $S_i$ & $\alpha_i S_i$ \crh
Sol &0.10 & 24 & 2.4\crh
Porte & 0.030& 2& 0.060\crh
Fenêtres & 0.030&6 & 0.18\crh
Murs &0.020 & 52& 1.04\crh
Plafond 1 &0.020 &24 & 0.48\crh
Plafond 2 & 0.75 & 24 & 18 \crh
& Total & \SI{108}{\meter\squared} & \SI{22.16}{\meter\squared} \crh
& & 10\% & 0.416\crh
& & $A_1$ & 22.58\crh

\end{tabularx}
\vspace{0.1cm}

on a donc $Tr_2 = \dfrac{0.16 \times 72}{22.58} \approx \SI{0.5}{\second}$. Ce temps est divisé par 5 donc oui.
\vspace{0.2cm}

\end{correction}
\finexo

\exo{}
Un compresseur situé dans un local de forme pavé de dimensions $L=\SI{3.0}{\meter}$, $l=\SI{2.0}{\meter}$ et $h=\SI{2.4}{\meter}$ est fermé par une porte de largeur $l_p=\SI{1.2}{\meter}$ et de hauteur $h_p=\SI{2.0}{\meter}$.\\
Les parois (plancher, plafond, murs) sont en béton tel que $\alpha=0.040$. La porte est en bois de coefficient $\alpha_2= 0.090$.\\
A l'aide d'un sonomètre, on a analysé le bruit de ce compresseur pour les fréquences normalisées :
\vspace{0.3cm}


\renewcommand{\arraystretch}{1.5}
\begin{tabularx}{\textwidth}{|C|C|C|C|C|C|C|}
\hline
Fréquence (\si{\hertz}) & 125 & 250 & 500 & 1000 & 2000 & 4000 \crh
Niveau (\si{\decibel}) & 86 & 84 & 84 & 71 & 67 & 64\crh
\end{tabularx}
\vspace{0.1cm}


\begin{enumerate}
\item Rappeler l'expression du niveau $L$ en fonction de $I$ et $I_0$
\item Calculer le niveau acoustique global de ce compresseur
\item Rappeler la formule de Sabine
\item Calculer $Tr_1$

On traite le plafond le sol et les murs pour améliorer l'isolation. Le nouveau coefficient est $\alpha_3 = 0.68$.
\item Calculer $Tr_2$
\item Sachant qu'on peut modéliser la variation $\Delta L$ du niveau sonore globale par la relation {\large $\Delta L = 10\log {\dfrac{Tr_2}{Tr_1}}$}, calculer le nouveau niveau sonore global en fonctionnement.
\end{enumerate}
\begin{correction}
\begin{enumerate}
\item Voir cours
\item Voir cours pour la formule. AN : $L_t = 10 \times \log{ 10^{8.6} + 10^{8.4} + 10^{8.4} + 10^{7.1} + 10^{6.7} +10^{6.4}} = 89.6 \approx \SI{90}{\decibel}$
\item Calcul du volume : $V=3.0 \times 2.4 \times 2.0 = \SI{14.4}{\meter\cubed} = \SI{14}{\meter\cubed}$ \\
Calcul de A :  $ A = \alpha ( S_m + S_p + S_{planch}) + \alpha_2 S_{porte} = \SI{1.56}{\meter\squared}$ \\
Calcul de T : $T = \dfrac{0.16 \times 14.3}{1.56} \approx \SI{1.5}{\second}$

\item On reprend les résultats précédents mais on change les coefficients du mur $A_2 = \SI{23}{\meter\squared}$ et donc $T_2 \approx \SI{0.099}{\second}$.
\item $\Delta L = 10\log{ \dfrac{0.099}{1.5}} = -11.8 \approx \SI{-12}{\decibel}$. Le nouveau niveau sonore est donc de \SI{78}{\decibel}

\end{enumerate}

\end{correction}
\finexo

\vspace{0.1cm}

\exo{} On donne une séparation composée d'un mur et d'une porte dont les dimensions sont respectivement $S_1=\SI{20}{\meter\squared}$ et $S_2=\SI{4.0}{\meter\squared}$ et on a les valeurs des isolements bruts $R_1 = \SI{40}{\decibel}$ et $R_2=\SI{25}{\decibel}$.\\
D'un côté de ce mur le niveau d'intensité acoustique vaut $L=\SI{70}{\decibel}$ et on suppose dans un premier temps que le mur n'a pas de porte.

\begin{enumerate}
\item Déterminer le niveau sonore de l'autre côté du mur

Comme la paroi n'est pas homogène, on doit calculer le coefficient $\tau_{moyen}$

\item Établir l'expression {\Large $\tau_{moyen} = \dfrac{S_1 \times 10^{\frac{-R_1}{10}}+ S_2 \times 10^{\frac{-R_2}{10}}}{S_1+S_2}$}
\item Calculer $\tau_{moyen}$
\item Calculer $R_F$ l'isolement de l'ensemble \{ mur + porte \}. On arrondira à l'unité.
\item Évaluer la perte d'isolement due à la présence de la porte
\item Calculer le niveau sonore réel derrière l'ensemble.

\end{enumerate}
\begin{correction}
\begin{enumerate}
\item On a $L_i = 70-40=\SI{30}{\decibel}$ si on enlève la porte
\item On part des définitions de R et on démontre, voir cours. $R_1 = 10\log{\dfrac{1}{\tau_1}}$
\item $\tau_m = 6.1\times 10^{-4}$
\item $R_f=10\log{\dfrac{1}{\tau_m}} = \SI{32}{\decibel}$
\item La porte occasionne donc une perte sèche de \SI{8}{\decibel}
\item On reprend la Q1 et on a : $L_{i2} = 70-32=\SI{38}{\decibel}$
\end{enumerate}

\end{correction}
\finexo

\vspace{0.1cm}

\exo{} Soit un mur de surface $S=\SI{20}{\meter\squared}$ donnant sur une source $L=\SI{40}{\decibel}$ et d'épaisseur $e=\SI{22}{\centi\meter}$ de brique. Un double vitrage de $S_f=\SI{4.0}{\meter\squared}$ d'épaisseur 4-12-4 pour lequel on négligera la couche d'air complète le mur.\\
On donne $\rho_v = \SI{2500}{\kilo\gram\per\meter\cubed}$ et $\rho_b = \SI{2100}{\kilo\gram\per\meter\cubed}$.
\begin{enumerate}
\item Calculer la masse surfacique des éléments
\item Donner la relation de la loi des masses permettant de calculer l'indice d'affaiblissement
\item Calculer $R_B$ et $R_f$
\item Déterminer $R_{total}$ en expliquant
\item Calculer le niveau d'intensité acoustique dans le bâtiment
\item Évaluer la perte occasionnée par le vitrage.
\end{enumerate}
\begin{correction}

\begin{enumerate}
\item $\sigma_v = 8.10^{-3} \times 2500 = \SI{20}{\kilo\gram\per\meter\squared}$ et $\sigma_b = 22.10^{-2} \times 2100 = \SI{462}{\kilo\gram\per\meter\squared}$
\item Pour le vitrage on prendra $R=17\log{\sigma} + 4$ et pour la brique $R=40\log{\sigma} -46$
\item Voir cours : $R_f = \SI{26}{\decibel}$ et $R_b = \SI{61}{\decibel}$
\item On reprend la même chose que l'exercice précédent et on trouve $\tau_m = 5.03.10^{-4}$ et $R_t=\SI{33}{\decibel}$
\item On a donc $L_2 = L-33 = \SI{47}{\decibel}$
\item La perte avec la vitre est donc $61-33=\SI{28}{\decibel}$ ce qui est important.
\end{enumerate}

\end{correction}
\finexo

\fi
\ifsectioncorrige
\newpage
\begin{center}Corrigé des exercices\end{center}
\setcounter{page}{1}
\Closesolutionfile{mycor}
\Readsolutionfile{mycor}


\fi

\end{document}