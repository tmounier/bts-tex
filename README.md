# Présentation

Cet espace se veut dédié au partage de mes ressources (réalisées en langage LaTeX) pour les cours de physique que je donne à ma classe de BTS bâtiment.

## Quelques informations

J'utilise dans la réalisation de documents des commandes personnalisées centralisées dans un "package" perso (deux versions en septembre 2023).
Ces fichiers sont disponibles aussi dans cet espace.
Je suis en train de migrer tous mes contenus en CC-BY mais j'ai un travail à faire sur les images utilisées parfois. 

## Réutilisation des ressources 

Je ne suis pas assez versé dans GIT pour gérer pour le moment des "forks" et des demandes d'intégration, alors voir ceci comme un simple espace de partage !


## Crédits

Merci à C.IACONELLI pour l'outil [md2html](https://ciaconelli.forge.aeif.fr/md2html/) qui permet un gain de temps dans la mise à jour de cette page ! :-)

Icône du projet : created by Icon Pond - Flaticon